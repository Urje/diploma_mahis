package controllers;

import models.Aimag;
import models.SumName;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

@With(Secure.class)
public class SumNames extends CRUD {

    public static void list(Long aid, int page, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        Aimag aimags = Aimag.findById(aid);
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        String where = "aimag.id=" + aid;
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, where);
        Long count = type.count(search, searchFields, where);
        Long totalCount = type.count(null, null, where);
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search, aimags);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order, aimags);
        }
    }

    public static void blank(Long aid) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        Aimag aimags = Aimag.findById(aid);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Model object = (Model) constructor.newInstance();
        try {
            render(type, object, aimags);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, aimags);
        }
    }

    public static void create(Long aid) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        SumName object = (SumName) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.aimag = Aimag.findById(aid);
        validation.valid(object);
        if (validation.hasErrors())
        {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(aid, 1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(aid);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void show(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        SumName object = SumName.findById(id);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        notFoundIfNull(object);
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object);
        }
    }

    public static void save(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        SumName object = SumName.findById(id);
        Aimag aimags = object.aimag;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.aimag = aimags;
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list(aimags.id, 1, null, null, null, null);
        }
        redirect(request.controller + ".show", object._key());
    }

}
