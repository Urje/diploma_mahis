package controllers;

import models.Bag;
import models.SumName;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

@With(Secure.class)
public class Bags extends CRUD {

    public static void list(Long sid, int page, String search, String searchFields, String orderBy, String order) {
     
        ObjectType type = ObjectType.get(getControllerClass());
        SumName sumName = SumName.findById(sid);
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        String where = "sumName.id=" + sid;
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, where);
        Long count = type.count(search, searchFields, where);
        Long totalCount = type.count(null, null, where);
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search, sumName);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order, sumName);
        }
    }

    public static void blank(Long sid) throws Exception {
        SumName sumName = SumName.findById(sid);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Model object = (Model) constructor.newInstance();
        try {
            render(type, object, sumName);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, sumName);
        }
    }

    public static void create(Long sid) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Bag object = (Bag) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.sumName = SumName.findById(sid);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(sid, 1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(sid);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void show(Long id) throws Exception {
        Bag object = Bag.findById(id);
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        notFoundIfNull(object);
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object);
        }
    }

    public static void save(Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Bag object = Bag.findById(id);
        SumName sumName = object.sumName;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.sumName= sumName;
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list(sumName.id, 1, null, null, null, null);
        }
        redirect(request.controller + ".show", object._key());
    }

}
