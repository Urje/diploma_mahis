package controllers;

import groovy.lang.Category;
import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends CRUD {

    public static void index()
    {
        List<Supplier> suppliers = Supplier.find("order by name").fetch();
        render(suppliers);
    }

}