package controllers;

        import models.ResearchCategory;
        import models.User;
        import play.data.binding.Binder;
        import play.exceptions.TemplateNotFoundException;
        import play.mvc.With;
        import play.db.Model;

        import java.lang.reflect.Constructor;
        import java.util.List;
        import java.lang.String;

@With(Secure.class)
public class ResearchCategorys extends CRUD {

    public static void list(int page, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, (String) request.args.get("where"));
        Long count = type.count(search, searchFields, (String) request.args.get("where"));
        Long totalCount = type.count(null, null, (String) request.args.get("where"));
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order);
        }
    }

    public static void create(Long id) throws Exception {
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        ResearchCategory object;
        if (id == null) object = (ResearchCategory) constructor.newInstance();
        else object = ResearchCategory.findById(id);

        Binder.bind(object, "object", params.all());

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, null, null, null, null);
        }
//        if (params.get("_saveAndAddAnother") != null) {
//            blank(id);
//        }
        redirect(request.controller + ".show", object._key());
    }




    public static void save(String id) throws Exception {

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        ResearchCategory object = (ResearchCategory) type.findById(id);
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, null, null, null, null);
        }

        redirect(request.controller + ".show", object._key());
    }
    public static void blank(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        ResearchCategory object;
        if (id == null) object = null;
        else object = ResearchCategory.findById(id);

        try {
            render(type, object, id);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, id);
        }
    }

    public static void show(Long id) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ResearchCategory object = ResearchCategory.findById(id);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        notFoundIfNull(object);
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object);
        }
    }
//    public static void delete(String id) throws Exception {
//        ObjectType type = ObjectType.get(getControllerClass());
//        notFoundIfNull(type);
//        ResearchCategory object = (ResearchCategory) type.findById(id);
//        notFoundIfNull(object);
//        try {
//            object._delete();
//        } catch (Exception e) {
//            flash.error(Messages.get("crud.delete.error", type.modelName));
//            redirect(request.controller + ".show");
//        }
//        flash.success(Messages.get("crud.deleted", type.modelName));
//        redirect(request.controller + ".list");
//    }

}
