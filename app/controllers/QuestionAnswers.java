package controllers;

import models.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

@With(Secure.class)
public class QuestionAnswers extends CRUD {

    public static void list(Long id, int page, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        Research research = Research.findById(id);
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        String where = "research.id=" + id;
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, where);
        Long count = type.count(search, searchFields, where);
        Long totalCount = type.count(null, null, where);
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search, research);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order, research);
        }
    }

    public static void blank(Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Research object;
        if (id == null) object = null;
        else object = Research.findById(id);
        try {
            render(type, object, id);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object,id);
        }
    }

    public static void create(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        QuestionAnswer object = (QuestionAnswer) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors())
        {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(id, 1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(id);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void show(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        Question object = Question.findById(id);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        notFoundIfNull(object);
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object);
        }
    }

    public static void save(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        QuestionAnswer object = QuestionAnswer.findById(id);
        Research researchs = object.research;
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.research = researchs;
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            list(researchs.id, 1, null, null, null, null);
        }
        redirect(request.controller + ".show", object._key());
    }

}
