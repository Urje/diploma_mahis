package controllers;

import models.User;


public class Security extends Secure.Security {

    static boolean authentify(String username, String password) {
        username = username.toLowerCase();
        User user = User.find("LOWER(username)=?1 AND active=true", username).first();
        if (user != null) {
            if (!user.username.equals(username)) {
                return false;
            }
            return user.password.compareTo(password) == 0;
        }
        return false;
    }

    static void onDisconnected() {
        Application.index();
    }

    static void onAuthenticated() {
        Aimags.list(1, null, null, null, null);
    }


}
