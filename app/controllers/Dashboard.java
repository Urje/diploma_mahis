package controllers;

import models.Aimag;
import models.Supplier;
import models.User;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

@With(Secure.class)
public class Dashboard extends CRUD {

    public static void list() {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();
        List<Supplier> suppliers = Supplier.find("order by name").fetch();
        render(suppliers);

    }
 }