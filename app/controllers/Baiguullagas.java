package controllers;

import models.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;


@With(Secure.class)
public class Baiguullagas extends CRUD {

    public static void list(int page, Long bid, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        Baiguullaga baiguullaga = null;
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        String where = " 1=1 ";
        if (bid != null) {
            baiguullaga = Baiguullaga.findById(bid);
            where += " AND baiguullaga.id=" + bid;
        }
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, (String) request.args.get("where"));
        List<Baiguullaga> baiguullagas = Baiguullaga.find("ORDER BY name").fetch();
        Long count = type.count(search, searchFields, (String) request.args.get("where"));
        Long totalCount = type.count(null, null, (String) request.args.get("where"));
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search,baiguullaga,baiguullagas);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order,baiguullaga, baiguullagas);
        }
    }
//
//        ObjectType type = ObjectType.get(getControllerClass());
//        notFoundIfNull(type);
//        String where = "";
//        if (bid != null) {
//            where += getWhere(where) + " baiguullaga.id=" + bid;
//        }
//        if (page < 1) {
//            page = 1;
//        }
//        if (orderBy == null) {
//            orderBy = "name";
//            order = "ASC";
//        }
//
//        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, where);
//        Long count = type.count(search, searchFields, where);
//        Long totalCount = type.count(null, null, where);
//        try {
//            render(type, objects, count, totalCount, page, orderBy, order, search);
//
//        } catch (TemplateNotFoundException e) {
//            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order);
//        }


    public static void blank(Long bid, Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);

        Baiguullaga rootBaiguulaga = null;
        if (bid != null) rootBaiguulaga = Baiguullaga.findById(bid);

        Baiguullaga object;
        if (id == null) object = null;
        else object = Baiguullaga.findById(id);

        List<Aimag> aimags = Aimag.find("order by name").fetch();
        List<SumName> sumNames = SumName.find("order by name").fetch();
        List<BTorol> bTorols = BTorol.find("order by name").fetch();
        try {
            render(type, rootBaiguulaga, object, id, aimags, sumNames,bTorols);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, rootBaiguulaga, object, id, aimags, sumNames,bTorols);
        }
    }

    public static void create(Long bid, Long id) throws Exception {
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Baiguullaga object;
        if (id == null) object = (Baiguullaga) constructor.newInstance();
        else object = Baiguullaga.findById(id);

        if (bid != null) {
            object.baiguullaga = Baiguullaga.findById(bid);
        }

        Binder.bind(object, "object", params.all());

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, bid, null, null, null, null);
        }
//        if (params.get("_saveAndAddAnother") != null) {
//            blank(id);
//        }
        redirect(request.controller + ".show", object._key());
    }

    public static void loadSumName(Long id) {
        List<SumName> sumNames = SumName.find("aimag.id=?1 ORDER BY name", id).fetch();
        render(sumNames);
    }

    public static void loadBagName(Long id) {
        List<Bag> bags = Bag.find("sumName.id=?1 ORDER BY name", id).fetch();
        System.out.println("dhddd" + bags.size());
        render(bags);
    }
}
