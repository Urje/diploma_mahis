package controllers;

import models.*;
import org.joda.time.DateTime;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import sun.security.pkcs11.wrapper.Functions;

import java.lang.reflect.Constructor;
import java.util.*;

public class Questions extends CRUD {

    public static void list(Long id, int page, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        Research research = Research.findById(id);
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        String where = "research.id=" + id;
        List<Question> objects = Question.find(where).fetch();
        Long count = type.count(search, searchFields, where);
        Long totalCount = type.count(null, null, where);
        System.out.println("hhs");

        Question question = Question.findById(id);
        List<Question> laterQuestions = Question.findAll();
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search, research,question, laterQuestions);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order, research,question,laterQuestions);
        }
    }


    public static void blank(Long id) throws Exception {
        User user = Users.getUser();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Question object;
        if (id == null) object = null;
        else object = Question.findById(id);

        Date nowDate = new Date();

        try {
            render(type, object, id, user);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, nowDate);
        }
    }

    public static void create(Long id) throws Exception {

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Question object;

        if (id == null) {
            object = (Question) constructor.newInstance();
            Calendar calendar = Calendar.getInstance();
        } else {
            object = Question.findById(id);
        }
    }

    public static void show(Long id) throws Exception {
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Question object = Question.findById(id);
        notFoundIfNull(object);
        try {
            render(type, object, user);
        } catch (TemplateNotFoundException e) {
            render("CRUD/show.html", type, object, user);
        }
    }


    public static void loadUser(Long tid) {
        List<User> users = User.find("userTeam.id = ?1 AND active=?2 ORDER BY firstName", tid, true).fetch();
        render(users);
    }
    public static void addQuestionType(Long sid, Long qt_id) {
        try {
            Research research = Research.findById(sid);
            QuestionType questionType = QuestionType.findById(qt_id);

            Question question = new Question();
            question.research = research;
            question.questionType = questionType;
            question.name = "";
            if (questionType.id >= 3) {
                question.questionOptions = new ArrayList<QuestionOption>();
                QuestionOption questionOption = new QuestionOption();
                questionOption.name = "";
                questionOption.question = question;
                question.questionOptions.add(questionOption);
            }
            question.create();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            renderText("error");
        }
        renderText("success");
    }


    public static void showResQuestion(Long sid) {
        Question question = Question.findById(sid);
        render(question);
    }

    public static void questionDetail(Long qid) {
        Question question = Question.findById(qid);
        List<Question> laterQuestions = Question.find("research.id = ?1 AND id > ?2", question.research.id, qid).fetch();
        render(question, laterQuestions);
    }

    public static void saveOption(Long questionId, String questionName, Long[] optionId, String[] optionName) {
        try {
            String isRequired = params.get("isRequired");
            Question question = Question.findById(questionId);
            question.name = questionName;
            question.save();

            if (QuestionType.count() >= 3) {
                for (int i = 0; i < optionId.length; i++) {
                    QuestionOption questionOption;
                    if (optionId[i] == 0) {
                        questionOption = new QuestionOption();
                    } else {
                        questionOption = QuestionOption.findById(optionId[i]);
                    }
                    questionOption.name = optionName[i];
                    questionOption.save();
                }
            }
        } catch (Exception ex) {
            renderText("error");
        }
        renderText("success");
    }

    public static void deleteOption(Long oid) {
        try {
            QuestionOption question = QuestionOption.findById(oid);
            question.delete();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            renderText("error");
        }
        renderText("success");
    }


    public static void deleteQuestion(Long qid) {
        try {
            Question question = Question.findById(qid);
            question.delete();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            renderText("error");
        }
        renderText("success");
    }


    public static void researchDelete(Long id) {
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Question object = Question.findById(id);
        object.save();
        flash.success(play.i18n.Messages.get("crud.deleted", type.modelName));
    }

    public static void changeStatus(Long sid) {
        try {
                Research research = Research.findById(sid);
                research.save();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            renderText("error");
        }
        renderText("success");
    }
}
