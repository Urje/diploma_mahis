package controllers;

import models.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Created by mungu on 2018-10-12.
 */
@With(Secure.class)
public class Suppliers extends CRUD {

    public static void list(Long id, int page, String search, String searchFields, String orderBy, String order) {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
        List<SumName> sumNames = SumName.find("aimag.id=?1 ORDER BY name", id).fetch();
        render(sumNames);

        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, (String) request.args.get("where"));
        Long count = type.count(search, searchFields, (String) request.args.get("where"));
        Long totalCount = type.count(null, null, (String) request.args.get("where"));
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order);
        }
    }

    public static void create(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Supplier object;
        if (id == null) object = (Supplier) constructor.newInstance();
        else object = Supplier.findById(id);

        Binder.bind(object, "object", params.all());

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(id);
        }
        redirect(request.controller + ".show", object._key());
    }


    public static void blank(Long id) throws Exception {
        User user = Users.getUser();
        if (user == null || (user != null && user.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Supplier object;
        if (id == null) object = null;
        else object = Supplier.findById(id);

        List<Aimag> aimags = Aimag.findAll();
        try {
            render(type, object, id, aimags);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, id, aimags);
        }
    }

    public static void loadSumName(Long id) {

    }

    public static void loadBagName(Long id) {
        List<Bag> bags = Bag.find("sumName.id=?1 ORDER BY name", id).fetch();
        render(bags);
    }

}
