package controllers;

import models.*;
import org.joda.time.DateTime;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

@With(Secure.class)
public class Researchs extends CRUD {

    public static void list(int page, String search, String searchFields, String orderBy, String order) {
        User u = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "name";
            order = "ASC";
        }
         List<Integer> years = new ArrayList<>();
        Year y = Year.now();
        int endYear = Integer.parseInt(y.toString());
        for (int i = endYear; i >= 2018; i--) {
            years.add(i);
        }

        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, (String) request.args.get("where"));
        Long count = type.count(search, searchFields, (String) request.args.get("where"));
        Long totalCount = type.count(null, null, (String) request.args.get("where"));
        try {
            render(years, type, objects, count, totalCount, page, orderBy, order, search);

        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", years, type, objects, count, totalCount, page, orderBy, order);
        }
    }
    public static void blank(Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Research object;
//        List<ResearchCategory> researchCategory = ResearchCategory.find("order by name").fetch();
        List<ResearchState> researchState = ResearchState.find("order by tolov").fetch();
        if (id == null) object = null;
        else object = Research.findById(id);
        try {
            render(type, object, id,researchState);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, id,researchState);
        }
        if (id == null) {
            object.create();
        } else {
            object.save();
        }

    }
    public static void create(Long id) throws Exception {
        User u = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Research object;
        if (id == null) object = (Research) constructor.newInstance();
        else object = Research.findById(id);

        Binder.bind(object, "object", params.all());
        System.out.println("---"+validation.hasErrors());
        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));

            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);

            }
        }
        if (id == null) {
            object.create();
        } else {
            object.save();
        }

        object.u = Users.getUser();
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(id);
        }
        redirect(request.controller + ".show", object._key());
    }



    public static void saveOption(Long questionId, String questionName, Long[] optionId, String[] optionName) {
        try {
            String isRequired = params.get("isRequired");
            Question question = Question.findById(questionId);
            question.name = questionName;
            question.isRequired = isRequired != null && isRequired.equals("on") ? true : false;
            question.save();

            if (question.questionType.id >= 3) {
                for (int i = 0; i < optionId.length; i++) {
                    QuestionOption questionOption;
                    if (optionId[i] == 0) {
                        questionOption = new QuestionOption();
                    } else {
                        questionOption = QuestionOption.findById(optionId[i]);
                    }
                    questionOption.name = optionName[i];
                    questionOption.question = question;
                    questionOption.save();
                }
            }
        } catch (Exception ex) {
            renderText("error");
        }
        renderText("success");
    }
}

