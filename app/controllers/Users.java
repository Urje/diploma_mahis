package controllers;

import models.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.List;

//import static controllers.Catalogs.getWhere;

//@With(Secure.class)
public class Users extends CRUD {

    public static User getUser() {
        return (User) renderArgs.get("user");
    }

    public static void list(int page, String search, String searchFields, String orderBy, String order) {
        User u = Users.getUser();
        if (u == null || (u != null && u.userType != 1)) forbidden();

        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        if (page < 1) {
            page = 1;
        }
        if (orderBy == null) {
            orderBy = "firstName";
            order = "ASC";
        }
        List<Model> objects = type.findPage(page, search, searchFields, orderBy, order, (String) request.args.get("where"));
        Long count = type.count(search, searchFields, (String) request.args.get("where"));
        Long totalCount = type.count(null, null, (String) request.args.get("where"));
        try {
            render(type, objects, count, totalCount, page, orderBy, order, search);
        } catch (TemplateNotFoundException e) {
            render("CRUD/list.html", type, objects, count, totalCount, page, orderBy, order);
        }
    }

    public static void create(Long id) throws Exception {
        User user = Users.getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        User object;
        if (id == null) object = (User) constructor.newInstance();
        else object = User.findById(id);

        Binder.bind(object, "object", params.all());

        if (validation.hasErrors()) {
            renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        flash.success(play.i18n.Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            list(1, null, null, null, null);
        }
        if (params.get("_saveAndAddAnother") != null) {
            blank(id);
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void show(Long id) {
        User u = Users.getUser();
        if (u == null || (u.id != id && u.userType != 1)) forbidden();
        User object = User.findById(id);
        render(object);
    }

    public static void save(String id) throws Exception {
        User owner = getUser();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        User object = User.findById(Long.parseLong(id));
        notFoundIfNull(object);
        String beforePassword = object.password;
        String username = object.username;
        Binder.bind(object, "object", params.all());

        validation.valid(object);
        object.password = beforePassword;
        object.username = object.username.toLowerCase();
        long count = User.find("id!=?1 AND username=?2", object.id, object.username).fetch().size();
        if (validation.hasErrors() || count > 0) {
            if (count > 0) renderArgs.put("error", "Нэвтрэх нэр давхардаж байна!");
            else renderArgs.put("error", Messages.get("crud.hasErrors"));
            User usercheck = object;
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object, usercheck);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();

        if (!username.equals(object.username) && owner.id.compareTo(object.id) == 0) {
            redirect("/logout");
        } else flash.success(Messages.get("crud.saved", object));

        redirect(request.controller + ".show", object._key());
    }
    public static int lastNameWordLength(boolean isnew, User user) {
        if (user.lastName == null) return 0;
        List<User> users;
        if (isnew) users = User.find("active=true").fetch();
        else users = User.find("active=true and id!=?1", user.id).fetch();
        int count = 1;
        while (count <= user.lastName.length() && count <= 3) {
            if (lastNameMatch(users, user, count)) count++;
            else break;
        }
        if (count > user.lastName.length()) return user.lastName.length();
        return count;
    }

    public static boolean lastNameMatch(List<User> users, User own, int count) {
        for (User user : users) {
            if (user.firstName != null && user.firstName.toLowerCase().equals(own.firstName.toLowerCase())
                    && user.lastName.toLowerCase().startsWith(own.lastName.toLowerCase().substring(0, count))) {
                return true;
            }
        }
        return false;
    }
    public static void blank(Long id) throws Exception {
        User u = Users.getUser();
        if (u == null || (u != null && u.userType != 1)) forbidden();
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        User object;
        if (id == null) object = null;
        else object = User.findById(id);
        List<Aimag> aimags = Aimag.find("order by name").fetch();
        List<Role> roles = Role.find("order by name").fetch();
        List<BTorol> bTorols = BTorol.find("order by name").fetch();
        List<Baiguullaga> baiguullagas = Baiguullaga.find("order by name").fetch();


        try {
            render(type, object, id, aimags,roles,bTorols,baiguullagas);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object, id, aimags,roles,bTorols,baiguullagas);
        }

    }
    public static void sumName(Long id) {
        List<SumName> sumNames = SumName.find("aimag.id=?1 ORDER BY name", id).fetch();
        render(sumNames);
    }

    public static void bagName(Long id) {
        List<Bag> bags = Bag.find("sumName.id=?1 ORDER BY name", id).fetch();
        render(bags);
    }
    public static void listLoad(Long aimag, Long sumName, String search, int CurrentPageNumber){
        User user=Users.getUser();
        if(user==null||(user!=null && user.userType!=1)) forbidden();
        String qr="Select distinct c from tb_user c",ss ;
        String where=" 1=1 ";
//        if(aimag!=null && aimag>0){
//            where+=getWhere(where) + " c.aimag.id= " + aimag;
//        }
//        if(sumName!=null && sumName>0){
//            where+=getWhere(where)+" c.sumName.id= " + sumName;
//        }
//        if(search!=null && search.length()>0){
//            where+=getWhere(where) + " (c.lastName LIKE '%"+search+"%' OR c.firstName LIKE '%"+search+"%' OR c.username LIKE '%"+search+"%')";        }

        if (where.length() > 0) where = " WHERE " + where;
        qr = qr + where + " order by c.id ASC "  ;

        int pageLimit = 20;
        int totalSize = User.find(qr).fetch().size();
        int MaxPageNumber = totalSize / pageLimit;
        if (totalSize % pageLimit != 0) MaxPageNumber++;
        Date nowDate = new Date();
        System.out.println("qr: " +qr);
        List<User> users = User.find(qr).fetch(CurrentPageNumber, pageLimit);
        render(users, MaxPageNumber, CurrentPageNumber, nowDate);

    }
    public static void loadBaiguullagaName(Long id) {
        List<Baiguullaga> baiguullagas = Baiguullaga.find("baiguullagiinTorol.id=?1 ORDER BY name", id).fetch();
        render(baiguullagas);
    }


}
