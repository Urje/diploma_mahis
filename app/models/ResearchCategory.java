package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_research_category")
public class ResearchCategory extends Model {
    public String name;

    public String toString() {
        return name;
    }
}

//    @OneToMany(mappedBy = "researchCategory")
//    public List<Research> research;}
