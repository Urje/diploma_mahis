package models;

import controllers.Baiguullagas;
import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "tb_user")
public class User extends Model {

    public String lastName;

    public String firstName;

    @Required
    public String email;

    @Required
    public String phone;

    @Required
    public String username;

    @Required
    public String password;

    public boolean active = true;

    public int userType = 0;  // 0 - user, 1- admin

    @ManyToOne
    public Aimag aimag;

    @ManyToOne
    public SumName sumName;

    @ManyToOne
    public Bag bag;

    @ManyToOne
    public Role role;

    @ManyToOne
    public BTorol bTorol;

    @ManyToOne
    public Baiguullaga baiguullaga;
//
//    @OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
//    public List<User> users;



}
