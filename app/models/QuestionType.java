package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_question_type")
public class QuestionType extends Model {

    public String name;


    @OneToMany(mappedBy = "questionType", cascade = CascadeType.ALL)
    public List<Question> question;

    public String toString() {
        return name;
    }

}
