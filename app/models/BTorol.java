package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_btorol")
public class BTorol extends Model {

    public String name;

    public String toString() {
        return this.name;
    }


    @OneToMany(mappedBy = "bTorol")
    public List<User> user;

    @OneToMany(mappedBy = "bTorol")
    public List<Baiguullaga> baiguullaga;


}


