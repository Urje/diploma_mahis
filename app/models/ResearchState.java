package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_research_state")
public class ResearchState extends Model {
    public String tolov;

    @OneToMany(mappedBy = "researchState", cascade = CascadeType.ALL)
    public List<Research> research;

    public String toString() {
        return tolov;
    }
}
