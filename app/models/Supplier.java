package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by mungu on 2018-10-12.
 */
@Entity(name = "tb_supplier")
public class Supplier extends Model {

    public String name;

    @ManyToOne
    public Aimag aimag;

    @ManyToOne
    public SumName sumName;

    @ManyToOne
    public Bag bag;

    public String address;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    public List<Catalog> catalogs;

    public String toString() {
        return this.name;
    }

}
