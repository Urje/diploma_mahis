package models;

import controllers.CRUD;
import controllers.Users;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;


@Entity(name = "tb_research")
public class Research extends Model {

    public String name;
    public String toString() {
        return name;
    }

    @ManyToOne
    public User u;

    @CRUD.Hidden
    public Date startDate;

    @CRUD.Hidden
    public Date endDate;

    @ManyToOne
    public ResearchState researchState;

//    @ManyToOne
//    public ResearchCategory researchCategory;


    @OneToMany(mappedBy = "research", cascade = CascadeType.ALL)
    public List<Question> question;

    @OneToMany(mappedBy = "research", cascade = CascadeType.ALL)
    public List<QuestionAnswer> questionAnswer;


}
