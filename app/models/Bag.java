package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_bag")
public class Bag extends Model {

    public String name;

    public String longitude;
    public String latitude;

    public String toString() {
        return this.name;
    }

    @OneToMany(mappedBy = "bag")
    public List<User> user;

    @CRUD.Hidden
    @ManyToOne
    public SumName sumName;

    @OneToMany(mappedBy = "bag")
    public List<Baiguullaga> baiguullaga;

}
