package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
@Entity(name = "tb_baiguullaga")
public class Baiguullaga extends Model {

    public String name;
    public String aToo;
    public String registerNum;

    @ManyToOne
    public Aimag aimag;

    @ManyToOne
    public SumName sumName;

    @ManyToOne
    public Bag bag;

    @ManyToOne
    public Baiguullaga baiguullaga;

    @ManyToOne
    public BTorol bTorol;

    public String toString() {
        return this.name;
    }

    @OneToMany(mappedBy = "baiguullaga", cascade = CascadeType.ALL)
    public List<User> user;
}

