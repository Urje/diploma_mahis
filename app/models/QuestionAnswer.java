package models;

import com.sun.org.apache.xpath.internal.operations.Mod;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity(name = "tb_question_answer")
public class QuestionAnswer extends Model {

    @ManyToOne
    public Research research;

    public Date date;

}