package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity(name = "tb_sum_team")
public class SumTeam extends Model {

    @ManyToOne
    public SumName sumName;

    @ManyToOne
    public UserTeam userTeam;

    @ManyToOne
    public User owner;

    public long getSumTeamFarmerCount() {
        return SumTeamFarmer.count("deleteFlag = 0 AND sumTeam.id = ?", this.id);
    }

    public String toString(){
        return userTeam.name;
    }

}
