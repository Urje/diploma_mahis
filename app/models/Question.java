package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tb_question")
public class Question extends Model {

    @ManyToOne
    public Research research;

    @Required
    @Column(length = 65535)
    public String name;

    @ManyToOne
    public QuestionType questionType;

    public boolean isRequired;

    public String toString() {
        return name;
    }

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    public List<QuestionOption> questionOptions;
}
