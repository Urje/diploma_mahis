package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;


@Entity(name = "tb_sum")
public class SumName extends Model {

    public String name;
    public String longitude;
    public String latitude;

    public String toString() {
        return this.name;
    }
    @CRUD.Hidden
    @ManyToOne
    public Aimag aimag;

    @OneToMany(mappedBy = "sumName")
    public List<Bag> bags;

    @OneToMany(mappedBy = "sumName")
    public List<User> user;

    @OneToMany(mappedBy = "sumName")
    public List<Baiguullaga> baiguullaga;


}
