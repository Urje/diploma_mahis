package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_role")
public class Role extends Model {
    public String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    public List<User> user;

    public String toString() {
        return name;
    }

}
