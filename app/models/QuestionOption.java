package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tb_question_option")
public class QuestionOption extends Model {

    @Required
    @Column(length = 65535)
    public String name;

    @ManyToOne
    public Question question;

    public String toString() {
        return name;
    }

}
