package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tb_userteam")
public class UserTeam extends Model {

    @Required
    public String name;

    public String register;

    @ManyToOne
    public UserTeam userTeam;

    @ManyToOne
    public Aimag aimag;

    @ManyToOne
    public SumName sumName;

    @ManyToOne
    public Bag bag;

    public String toString() {
        return this.name;
    }

    public List<User> getUsers() {
        return User.find("userTeam.id=? and active=true ORDER BY firstName").fetch();
    }

    public List<SumTeam> getSumTeams() {
        return SumTeam.find("userTeam.id=? ORDER BY sumName.aimag.id").fetch();
    }

    @OneToMany(mappedBy = "userTeam")
    public List<UserTeam> userTeams;
}