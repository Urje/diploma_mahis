package models;

import play.db.jpa.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "tb_aimag")
public class Aimag extends Model {

    public String name;

    public Long queue;

    @OneToMany(mappedBy = "aimag", cascade = CascadeType.ALL)
    public List<SumName> sumNames;

    @OneToMany(mappedBy = "aimag", cascade = CascadeType.ALL)
    public List<User> user;

    @OneToMany(mappedBy = "aimag", cascade = CascadeType.ALL)
    public List<Baiguullaga> baiguullaga;

    public String toString() {
        return this.name;
    }

}
